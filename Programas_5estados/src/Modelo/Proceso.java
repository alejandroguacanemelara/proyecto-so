package Modelo;

public class Proceso {
    private int identificacion;
    private String nombre;
    private int memoria;
    private String estado;
    private int recurso;

    public Proceso(int identificacion, String nombre, int memoria, String estado, int recurso) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.memoria = memoria;
        this.estado = estado;
        this.recurso = recurso;
    }


    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMemoria() {
        return memoria;
    }

    public void setMemoria(int memoria) {
        this.memoria = memoria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getRecurso() {
        return recurso;
    }

    public void setRecurso(int recurso) {
        this.recurso = recurso;
    }
    
    
}
